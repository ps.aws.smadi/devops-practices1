FROM openjdk:8
EXPOSE 8070
COPY target/211118-*.jar /usr/local/app.jar
ENTRYPOINT java -jar -Dspring.profiles.active=h2 /usr/local/app.jar